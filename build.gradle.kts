plugins {
    java
    idea
    `java-library`
    `maven-publish`
    id("io.freefair.lombok") version "6.3.0"
    id("org.jetbrains.dokka") version "1.6.10"
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("org.openjfx.javafxplugin") version "0.0.10"
}

group = "com.runemate"
version = "1.20.7-SNAPSHOT"

repositories {
    maven("https://gitlab.com/api/v4/projects/10471880/packages/maven")
    mavenCentral()
    mavenLocal()
}

val bootClass by extra("com.runemate.client.boot.Boot")

val externalRepositoryUrl: String by project
val externalRepositoryCredentialsName: String? by project
val externalRepositoryCredentialsValue: String? by project

val runemate by configurations.creating {
    configurations["compileOnly"].extendsFrom(this)
    configurations["testCompileOnly"].extendsFrom(this)
    configurations["testImplementation"].extendsFrom(this)
}

dependencies {
    //Provided transitively at runtime
    runemate("com.runemate:runemate-client:4.0.27.0")
    runemate("com.google.code.gson:gson:2.8.9")
    runemate("org.jetbrains:annotations:22.0.0")
    runemate("com.google.guava:guava:31.0.1-jre")
    runemate("org.apache.commons:commons-lang3:3.12.0")
    runemate("org.apache.commons:commons-math3:3.6.1")
    runemate("org.apache.commons:commons-text:1.10.0")
    runemate("commons-io:commons-io:2.11.0")
    runemate(platform("com.fasterxml.jackson:jackson-bom:2.17.0"))
    runemate("com.fasterxml.jackson.module:jackson-module-kotlin")
    runemate("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")
    runemate("com.fasterxml.jackson.datatype:jackson-datatype-jdk8")

    implementation("org.json:json:20211205")
    implementation("org.jblas:jblas:1.2.5")
}

val javafxVersion = "20"
val javafxModules = arrayOf(
        "javafx.base", "javafx.fxml", "javafx.controls", "javafx.media", "javafx.web", "javafx.graphics", "javafx.swing"
)

val requiredOpens = arrayOf(
        "java.base/java.lang.reflect",
        "java.base/java.nio",
        "java.base/sun.nio.ch",
        "java.base/java.util.regex"
)

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

javafx {
    version = javafxVersion
    configuration = "runemate"
    modules(*javafxModules)
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
    withSourcesJar()
    withJavadocJar()
}

tasks.withType<Javadoc> {
    exclude("**/README.md")
    title = "RuneMate Game API $version"
    source = sourceSets.main.get().allJava

    options {
        showFromPublic()
        encoding = "UTF-8"
        (this as CoreJavadocOptions).addStringOption("Xdoclint:none", "-quiet")
    }
}

val launch = task("launch", JavaExec::class) {
    group = "runemate"
    classpath = files(runemate, tasks.shadowJar)
    mainClass.set(bootClass)
    requiredOpens.forEach { jvmArgs("--add-opens=${it}=ALL-UNNAMED") }
}

gradle.taskGraph.whenReady {
    if (hasTask("launch")) {
        tasks.withType<Javadoc>().forEach { it.enabled = false }
    }
}

tasks.register("testJar", Jar::class) {
    group = "build"
    from(sourceSets.test.get().output)
    archiveClassifier.set("test")
}

publishing {
    publications {
        register<MavenPublication>("maven") {
            from(components["java"])
        }
    }

    repositories.maven {
        name = "external"
        url = uri(externalRepositoryUrl)
        if (externalRepositoryCredentialsValue != null) {
            credentials(HttpHeaderCredentials::class) {
                name = externalRepositoryCredentialsName ?: "Private-Token"
                value = externalRepositoryCredentialsValue
            }
            authentication.create<HttpHeaderAuthentication>("header")
        }
    }
}
