# API Root

### Packages and restrictions

> Anything marked ✘ for "Live use" cannot be referenced in your SVN code.  
> Your bots will be rejected automatically.

| Package         | Description                       | Accepting MRs | Live use | Breaking Changes |
|:----------------|:----------------------------------|:-------------:|:--------:|:----------------:|
| `.api.*`        | Finalized game API                |      ✔*       |    ✔     |        ✘         |
| `.cache.*`      | Code for reading from game cache  |       ✔       |    ✔     |        ✘         |
| `.events.*`     | OSRS Game Event Handlers          |       ✔       |    ✔     |        ✘         |
| `.incubating.*` | Code not yet ready for live use   |       ✔       |    ✘     |        ✔         |
| `.internal.*`   | Protected internal code           |       ✔       |    ✘     |        ✔*        |

<sup>*</sup>As long as they don't break anything in `.api.*`.

Changes to code already in use on the bot store (packages marked ✔ for "Live use") cannot have changes which could cause
existing bots to break.