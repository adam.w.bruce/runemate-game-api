package com.runemate.game.api.hybrid.cache.configs;

import com.runemate.game.api.hybrid.cache.materials.*;
import java.awt.*;
import java.util.List;
import java.util.*;

public interface IdentityKit {
    int getId();

    int getBodyPartId();

    List<Integer> getModelIds();

    List<Integer> getHeadModelIds();

    Map<Color, Color> getColorSubstitutions();

    Map<Material, Material> getMaterialSubstitutions();
}
