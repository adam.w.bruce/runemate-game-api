package com.runemate.game.api.hybrid.cache.configs;

public interface InventoryDefinition {

    int getId();

    int getCapacity();

}
