package com.runemate.game.api.hybrid.cache.db;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.io.*;
import java.util.*;
import lombok.*;

@Data
public class DBTableIndex implements DecodedItem {

    private final int tableId;
    private final int columnId;
    private BaseType[] tupleTypes;
    private List<Map<Object, List<Integer>>> tupleIndexes;

    public DBTableIndex(final int tableId, final int columnId) {
        this.tableId = tableId;
        this.columnId = columnId;
    }

    @Override
    public void decode(final Js5InputStream stream) throws IOException {
        int tupleSize = stream.readVarInt2();
        BaseType[] tupleTypes = new BaseType[tupleSize];
        List<Map<Object, List<Integer>>> tupleIndexes = new ArrayList<>(tupleSize);

        for (int i = 0; i < tupleSize; i++) {
            tupleTypes[i] = BaseType.by(stream.readUnsignedByte());

            int valueCount = stream.readVarInt2();
            Map<Object, List<Integer>> valueToRows = new HashMap<>(valueCount);

            while (valueCount-- > 0) {
                Object value = decodeValue(tupleTypes[i], stream);

                int rowCount = stream.readVarInt2();
                List<Integer> rowIds = new ArrayList<>(rowCount);

                while (rowCount-- > 0) {
                    rowIds.add(stream.readVarInt2());
                }

                valueToRows.put(value, rowIds);
            }

            tupleIndexes.add(i, valueToRows);
        }

        setTupleTypes(tupleTypes);
        setTupleIndexes(tupleIndexes);
    }

    private static Object decodeValue(BaseType baseType, Js5InputStream stream) throws IOException {
        switch (baseType) {
            case INT:
                return stream.readInt();
            case LONG:
                return stream.readLong();
            case STRING:
                return stream.readCStyleLatin1String();
        }
        return null;
    }

    public static Object[] decodeColumnFields(Js5InputStream stream, CS2VarType[] types) throws IOException {
        int fieldCount = stream.readShortSmart();
        Object[] values = new Object[fieldCount * types.length];

        for (int fieldIndex = 0; fieldIndex < fieldCount; fieldIndex++)
        {
            for (int typeIndex = 0; typeIndex < types.length; typeIndex++)
            {
                CS2VarType type = types[typeIndex];
                int valuesIndex = fieldIndex * types.length + typeIndex;

                if (type == CS2VarType.STRING)
                {
                    values[valuesIndex] = stream.readCStyleLatin1String();
                }
                else
                {
                    values[valuesIndex] = stream.readInt();
                }
            }
        }

        return values;
    }
}
