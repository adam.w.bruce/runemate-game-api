package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.io.*;
import lombok.*;

@Getter
@RequiredArgsConstructor
public class CacheInventoryDefinition extends IncrementallyDecodedItem implements InventoryDefinition {

    private final int id;
    private int capacity;

    @Override
    protected void decode(final Js5InputStream stream, final int opcode) throws IOException {
        if (opcode == 2) {
            this.capacity = stream.readUnsignedShort();
        }
    }
}
