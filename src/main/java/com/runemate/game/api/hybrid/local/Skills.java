package com.runemate.game.api.hybrid.local;

import com.runemate.client.game.open.*;

/**
 * A utility for manipulating generic skill data.
 */
public final class Skills {
    private static final int[] STANDARD_EXPERIENCE_TABLE = new int[120];
    private static final int[] ELITE_EXPERIENCE_TABLE =
        new int[] {
            0, 830, 1861, 2902, 3980, 5126, 6390, 7787, 9400, 11275, 13605,
            16372, 19656, 23546, 28138, 33520, 39809, 47109, 55535, 64802, 77190, 90811, 106221,
            123573, 143025, 164742,
            188893, 215651, 245196, 277713, 316311, 358547, 404634, 454796, 509259, 568254, 632019,
            700797, 774834, 854383,
            946227, 1044569, 1149696, 1261903, 1381488, 1508756, 1644015, 1787581, 1939773, 2100917,
            2283490, 2476369,
            2679907, 2894505, 3120508, 3358307, 3608290, 3870846, 4146374, 4435275, 4758122,
            5096111, 5449685, 5819299,
            6205407, 6608473, 7028964, 7467354, 7924122, 8399751, 8925664, 9472665, 10041285,
            10632061, 11245538, 11882262,
            12542789, 13227679, 13937496, 14672812, 15478994, 16313404, 17176661, 18069395,
            18992239, 19945833, 20930821,
            21947856, 22997593, 24080695, 25259906, 26475754, 27728955, 29020233, 30350318,
            31719944, 33129852, 34580790,
            36073511, 37608773, 39270442, 40978509, 42733789, 44537107, 46389292, 48291180,
            50243611, 52247435, 54303504,
            56412678, 58575823, 60793812, 63067521, 65397835, 67785643, 70231841, 72737330,
            75303019, 77929820, 80618654
        };

    static {
        int accumulater = 0;
        for (int i = 1; i < 120; ++i) {
            accumulater = accumulater + (int) (i + 300.0D * StrictMath.pow(2.0D, i / 7.0D));
            STANDARD_EXPERIENCE_TABLE[i] = accumulater / 4;
        }
    }

    private Skills() {
    }

    public static Skill getByIndex(int index) {
        for (Skill skill : Skill.values()) {
            if (skill.getIndex() == index) {
                return skill;
            }
        }
        return null;
    }

    public static int[] getExperiences() {
        return OpenSkills.getCurrentExps();
    }

    public static int[] getBaseLevels() {
        return OpenSkills.getBaseLevels();
    }

    public static int[] getCurrentLevels() {
        return OpenSkills.getCurrentLevels();
    }

    /**
     * Gets the experience at a given level. If the data isn't loaded, returns -1
     */
    public static int getExperienceAt(int level) {
        return getExperienceAt(Skill.DUNGEONEERING, level);
    }

    /**
     * Gets the experience at a given level for the given skill. If the data isn't loaded, returns -1
     */
    public static int getExperienceAt(Skill skill, int level) {
        if (level < 1) {
            return 0;
        } else if (skill.isEliteSkill()) {
            if (level > ELITE_EXPERIENCE_TABLE.length) {
                level = ELITE_EXPERIENCE_TABLE.length;
            }
            return ELITE_EXPERIENCE_TABLE[level - 1];
        } else {
            if (level > STANDARD_EXPERIENCE_TABLE.length) {
                level = STANDARD_EXPERIENCE_TABLE.length;
            }
            return STANDARD_EXPERIENCE_TABLE[level - 1];
        }
    }

    public static int getLevelAtExperience(Skill skill, int experience) {
        int[] table = skill.isEliteSkill() ? ELITE_EXPERIENCE_TABLE : STANDARD_EXPERIENCE_TABLE;
        int level = 1;
        for (int i = 1; i < table.length; i++) {
            int experienceAtLevel = table[i];
            if (experience < experienceAtLevel) {
                level = i;
                break;
            }
        }
        return Math.min(level, skill.getMaxLevel());
    }
}
