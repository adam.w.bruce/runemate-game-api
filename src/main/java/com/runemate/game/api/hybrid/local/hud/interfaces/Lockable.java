package com.runemate.game.api.hybrid.local.hud.interfaces;

public interface Lockable {
    boolean isLocked();
}
