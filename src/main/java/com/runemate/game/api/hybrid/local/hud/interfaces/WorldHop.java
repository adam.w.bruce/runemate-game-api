package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import lombok.*;

/**
 * For using the in-game world hopping interface.
 * For the lobby, see com.runemate.game.api.hybrid.local.hud.interfaces.WorldSelect
 *
 * <p>
 * <p>
 * Updates
 * 16/03/2017 - Fixed issue where RuneMate would not detect world container because of missing height value (thanks for warrior55 for debugging)
 **/
public final class WorldHop {

    /**
     * Hops to a specified world using the quick-hop
     *
     * @param worldId world to hop to eg 70
     * @return return true if successfully hop to world, false if world id is not a valid world
     */
    public static boolean hopTo(int worldId) {
        return validate(worldId) && (OSRSWorldHop.to(worldId));
    }

    /**
     * Hops to a specified world using the quick-hop
     *
     * @param worldOverview WorldOverview object to identify world by
     * @return return true if successfully hop to world, false if world id is not a valid world
     */
    public static boolean hopTo(WorldOverview worldOverview) {
        return worldOverview != null && hopTo(worldOverview.getId());
    }

    /**
     * Hops to a specified world using the quick-hop
     *
     * @param filter Predicate to identify world with
     * @return return true if successfully hop to world, false if world id is not a valid world
     */
    public static boolean hopToFirst(Predicate<WorldOverview> filter) {
        return hopTo(Worlds.newQuery().filter(filter).results().first());
    }

    /**
     * Hop to random world using filter
     *
     * @param filter {@link Predicate} to filter worlds by
     * @return if hop was successful
     */
    public static boolean hopToRandom(Predicate<WorldOverview> filter) {
        return hopTo(Worlds.newQuery().filter(filter).results().random());
    }

    /**
     * Hop to random world, true = members-only, false = free-to-play
     *
     * @param membersOnly true = members-only, false = free-to-play
     * @return if hop was successful
     */
    public static boolean hopToRandom(boolean membersOnly) {
        int current = Worlds.getCurrent();
        return hopToRandom(world -> isSafeAndUsable(world, membersOnly, current));
    }

    /**
     * If world-hop interface is open
     *
     * @return if world-hop interface is open
     */
    public static boolean isOpen() {
        return OSRSWorldHop.isOpen();
    }

    /**
     * Opens world-hop interface
     *
     * @return if world hop interface is opened or already open
     */
    public static boolean open() {
        return OSRSWorldHop.open();
    }

    private static boolean validate(int worldId) {
        return Worlds.getOverview(worldId) != null;
    }

    public static boolean close() {
        return !WorldHop.isOpen()
            || (OSRSWorldHop.close())
            && Execution.delayWhile(WorldHop::isOpen, 600, 1200);
    }

    private static boolean isSafeAndUsable(@NonNull WorldOverview destination, boolean member, int current) {
        final var totalLevel = IntStream.of(Skills.getBaseLevels()).sum();
        final var types = destination.getWorldTypes();
        return destination.getId() != current
            && destination.isMembersOnly() == member
            && !types.contains(WorldType.PVP_ARENA)
            && !types.contains(WorldType.QUEST_SPEEDRUNNING)
            && !types.contains(WorldType.TOURNAMENT_WORLD)
            && !types.contains(WorldType.SEASONAL)
            && !types.contains(WorldType.LAST_MAN_STANDING)
            && !types.contains(WorldType.BETA)
            && !WorldType.isPvp(destination.getWorldTypes())
            && (!destination.isSkillTotal2200() || totalLevel >= 2200)
            && (!destination.isSkillTotal2000() || totalLevel >= 2000)
            && (!destination.isSkillTotal1750() || totalLevel >= 1750)
            && (!destination.isSkillTotal1500() || totalLevel >= 1500)
            && (!destination.isSkillTotal1250() || totalLevel >= 1250)
            && (!destination.isSkillTotal750() || totalLevel >= 750)
            && (!destination.isSkillTotal500() || totalLevel >= 500);
    }
}