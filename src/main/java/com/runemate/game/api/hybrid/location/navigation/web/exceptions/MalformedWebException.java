package com.runemate.game.api.hybrid.location.navigation.web.exceptions;

public class MalformedWebException extends RuntimeException {
    public MalformedWebException(String message) {
        super(message);
    }
}
