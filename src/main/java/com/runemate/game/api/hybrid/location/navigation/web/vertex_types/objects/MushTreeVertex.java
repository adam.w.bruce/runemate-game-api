package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.objects;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.apache.commons.lang3.builder.*;

@Log4j2
public class MushTreeVertex extends ObjectVertex implements SerializableVertex {

    private String text;

    public MushTreeVertex(Coordinate position, String text, Collection<WebRequirement> requirements) {
        super(position, (Pattern) null, (Pattern) null, requirements);
        this.text = text;
    }

    public MushTreeVertex(
        Coordinate position,
        Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements,
        int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    @Override
    public Pattern getTargetPattern() {
        if (this.target == null) {
            this.target = Regex.getPatternForExactStrings("Magic Mushtree");
        }
        return target;
    }

    @Override
    public Pattern getActionPattern() {
        if (this.action == null) {
            this.action = Regex.getPatternForExactStrings("Use");
        }
        return action;
    }


    @Override
    public int getOpcode() {
        return 20;
    }

    @Override
    @SneakyThrows(IOException.class)
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(text);
        return true;
    }

    @Override
    @SneakyThrows(IOException.class)
    public boolean deserialize(int protocol, ObjectInput stream) {
        text = stream.readUTF();
        return true;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getPosition())
            .append(getTargetPattern().pattern())
            .append(getActionPattern().pattern())
            .append(text)
            .toHashCode();
    }

    @Override
    public boolean step() {
        InterfaceComponent component = getOSRSInterfaceComponent();
        if (component == null) {
            GameObject object = getObject();
            if (object != null && (object.isVisible() || Camera.turnTo(object)) && object.interact(action, target)) {
                Player avatar = Players.getLocal();
                if (avatar != null && !Execution.delayUntil(() -> getOSRSInterfaceComponent() != null, avatar::isMoving, 1200, 2400)) {
                    return false;
                }
                component = getOSRSInterfaceComponent();
            }
        }
        if (component != null) {
            int plane = Scene.getCurrentPlane();
            Coordinate base = Scene.getBase(plane);
            if (component.interact("Continue") || component.click()) {
                return Execution.delayWhile(() -> base.equals(Scene.getBase(plane)), 4200, 6000);
            }
        }
        return false;
    }

    private InterfaceComponent getOSRSInterfaceComponent() {
        return Interfaces.newQuery().containers(608).types(InterfaceComponent.Type.LABEL).textContains(text).visible().results().first();
    }

    @Override
    public GameObject getObject() {
        final GameObjectQueryBuilder builder = GameObjects.newQuery().within(new Area.Circular(getPosition(), 5));
        if (target != null) {
            builder.names(target);
        }
        if (action != null) {
            builder.actions(action);
        }
        if (type != null) {
            builder.types(type);
        }
        if (provider != null) {
            builder.provider(() -> provider);
        }
        LocatableEntityQueryResults<GameObject> results = builder.results();
        if (results.size() > 1) {
            log.warn(results.size() + " results when querying " + this);
        }
        GameObject object = results.first();
        if (type == null && object != null) {
            type = object.getType();
        }
        return object;
    }

    @Override
    public String toString() {
        return "MushTreeVertex(name="
            + getTargetPattern().pattern()
            + ", action="
            + getActionPattern().pattern()
            + ", destination="
            + text
            + ')';
    }
}
