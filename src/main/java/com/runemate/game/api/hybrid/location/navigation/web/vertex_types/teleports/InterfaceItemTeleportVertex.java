package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.teleports;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import lombok.*;

public class InterfaceItemTeleportVertex extends ItemTeleportVertex {
    private SpriteItem.Origin origin;
    private Pattern itemName, itemAction, interfaceAction;
    private Integer container, child, grandchild, interfaceSpriteId, interfaceProjectedBufferId;
    private Color interfaceTextColor;
    //queryable attributes
    private String interfaceName;

    public InterfaceItemTeleportVertex(
        final Coordinate destination,
        final SpriteItem.Origin origin, final Pattern itemName,
        final Pattern itemAction,
        final Integer container, final Integer child,
        final Integer grandchild, final Pattern interfaceAction,
        final Collection<WebRequirement> requirements
    ) {
        super(destination, requirements);
        this.origin = origin;
        this.itemName = itemName;
        this.itemAction = itemAction;
        this.container = container;
        this.child = child;
        this.grandchild = grandchild;
        this.interfaceAction = interfaceAction;
    }

    public InterfaceItemTeleportVertex(
        final Coordinate destination,
        final SpriteItem.Origin origin, final Pattern itemName,
        final Pattern itemAction,
        final Integer container, final Integer child,
        final Pattern interfaceAction,
        final Collection<WebRequirement> requirements
    ) {
        this(destination, origin, itemName, itemAction, container, child, null, interfaceAction,
            requirements
        );
    }

    public InterfaceItemTeleportVertex(
        final Coordinate destination,
        final SpriteItem.Origin origin, final String itemName,
        final String itemAction,
        final Integer container, final Integer child,
        final Integer grandchild, final String interfaceAction,
        final Collection<WebRequirement> requirements
    ) {
        this(destination, origin, Regex.getPatternForExactString(itemName),
            Regex.getPatternForExactString(itemAction), container, child, grandchild,
            Regex.getPatternForExactString(interfaceAction), requirements
        );
    }

    public InterfaceItemTeleportVertex(
        final Coordinate destination,
        final SpriteItem.Origin origin, final String itemName,
        final String itemAction,
        final Integer container, final Integer child,
        final String interfaceAction,
        final Collection<WebRequirement> requirements
    ) {
        this(destination, origin, Regex.getPatternForExactString(itemName),
            Regex.getPatternForExactString(itemAction), container, child, null,
            Regex.getPatternForExactString(interfaceAction), requirements
        );
    }

    public InterfaceItemTeleportVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements,
        int protocol, ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    @Override
    public SpriteItem getItem() {
        SpriteItemQueryBuilder builder = null;
        if (origin == SpriteItem.Origin.INVENTORY) {
            builder = Inventory.newQuery();
        } else if (origin == SpriteItem.Origin.EQUIPMENT) {
            builder = Equipment.newQuery();
        }
        if (builder == null) {
            throw new UnsupportedOperationException("SpriteItems with an origin of " + origin +
                " are not currently supported by this vertex");
        }
        if (itemName != null) {
            builder.names(itemName);
        }
        if (itemAction != null) {
            builder.actions(itemAction);
        }
        if (provider != null) {
            builder.provider(() -> provider);
        }
        return builder.results().sortByIndex().first();
    }

    @Override
    public void invalidateCache() {
        provider = null;
    }

    @Override
    public Predicate<SpriteItem> getFilter() {
        SpriteItemQueryBuilder builder = null;
        if (origin == SpriteItem.Origin.INVENTORY) {
            builder = Inventory.newQuery();
        } else if (origin == SpriteItem.Origin.EQUIPMENT) {
            builder = Equipment.newQuery();
        }
        if (builder == null) {
            throw new UnsupportedOperationException("SpriteItems with an origin of " + origin +
                " are not currently supported by this vertex");
        }
        if (itemName != null) {
            builder.names(itemName);
        }
        if (itemAction != null) {
            builder.actions(itemAction);
        }
        return builder::accepts;
    }


    @Override
    public boolean step() {
        InterfaceComponent component;
        if (interfaceTextColor != null || interfaceSpriteId != null || interfaceName != null ||
            interfaceProjectedBufferId != null) {
            InterfaceComponentQueryBuilder builder = Interfaces.newQuery();
            if (container != null) {
                builder.containers(container);
            }
            if (interfaceTextColor != null) {
                builder.textColors(interfaceTextColor);
            }
            if (interfaceName != null) {
                builder.names(interfaceName);
            }
            if (interfaceProjectedBufferId != null) {
                builder.projectedBufferId(interfaceProjectedBufferId);
            }
            component = builder.results().first();
        } else if (grandchild != null) {
            component = Interfaces.getAt(container, child, grandchild);
        } else {
            component = Interfaces.getAt(container, child);
        }
        if (component != null && component.isVisible()) {
            return component.interact(interfaceAction)
                && Execution.delayWhile(component::isVisible, 1200, 3000);
        } else {
            final SpriteItem item = getItem();
            return item != null
                && (!Bank.isOpen() || Bank.close())
                && (!GrandExchange.isOpen() || GrandExchange.close())
                && item.interact(itemAction)
                && Execution.delayWhile(item::isVisible, 1200, 3000);
        }
    }

    @Override
    public int getOpcode() {
        return 17;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(origin.name());
        stream.writeUTF(itemName.pattern());
        stream.writeInt(itemName.flags());
        stream.writeUTF(itemAction.pattern());
        stream.writeInt(itemAction.flags());
        stream.writeUTF(interfaceAction.pattern());
        stream.writeInt(interfaceAction.flags());
        stream.writeInt(container);
        stream.writeInt(child);
        stream.writeInt(grandchild != null ? grandchild : -1);
        //stream.writeInt(interfaceProjectedBufferId != null ? interfaceProjectedBufferId : -1);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.origin = SpriteItem.Origin.valueOf(stream.readUTF());
        this.itemName = Pattern.compile(stream.readUTF(), stream.readInt());
        this.itemAction = Pattern.compile(stream.readUTF(), stream.readInt());
        this.interfaceAction = Pattern.compile(stream.readUTF(), stream.readInt());
        this.container = stream.readInt();
        this.child = stream.readInt();
        this.grandchild = stream.readInt();
        if (grandchild == -1) {
            grandchild = null;
        }
        /*this.interfaceProjectedBufferId = stream.readInt();
        if (interfaceProjectedBufferId == -1) {
            interfaceProjectedBufferId = null;
        }*/
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(origin, itemName, itemAction, interfaceAction, container, child,
            grandchild
        );
    }

    @Override
    public String toString() {
        Coordinate pos = getPosition();
        return "InterfaceItemTeleportVertex(origin=" + origin + ", itemName=" + itemName.pattern() +
            ", itemAction=" + itemAction.pattern() +
            ", interfaceAction=" + interfaceAction.pattern() + ", component=" + container + "," +
            child + (grandchild != null ? "," + grandchild : "") +
            ", x=" + pos.getX() + ", y=" + pos.getY() + ", plane=" + pos.getPlane();
    }
}
