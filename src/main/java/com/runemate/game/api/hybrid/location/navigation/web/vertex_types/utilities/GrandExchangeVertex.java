package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.utilities;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.collections.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import org.jetbrains.annotations.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class GrandExchangeVertex extends UtilityVertex implements SerializableVertex {
    protected Pattern name;

    protected GrandExchangeVertex(Coordinate position, Collection<WebRequirement> requirements) {
        super(position, requirements);
    }

    public GrandExchangeVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    public GrandExchangeVertex(
        final String name, final Coordinate position,
        final Collection<WebRequirement> requirements
    ) {
        this(Regex.getPatternForExactString(name), position, requirements);
    }

    public GrandExchangeVertex(
        final Pattern name, final Coordinate position,
        final Collection<WebRequirement> requirements
    ) {
        super(position, requirements);
        this.name = name;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getPosition()).append(getName()).toHashCode();
    }

    public Npc getClerk() {
        return Npcs.newQuery().on(getPosition()).names(name).results().first();
    }

    @Override
    public boolean step() {
        return (!Bank.isOpen() || Bank.close()) && GrandExchange.open(getClerk(), "Exchange");
    }

    @Override
    public boolean step(boolean prefersViewport) {
        return step();
    }

    @NonNull
    @Override
    public Pair<WebVertex, WebPath.VertexSearchAction> getStep(Map<String, Object> args) {
        final Npc clerk = getClerk();
        if (clerk != null && clerk.isVisible()) {
            return new Pair<>(this, WebPath.VertexSearchAction.STOP);
        } else {
            return new Pair<>(
                (WebVertex) args.getOrDefault(
                    WebVertex.PREVIOUS,
                    new WebPath.PseudoCoordinateVertex(position, Collections.emptyList())
                ),
                WebPath.VertexSearchAction.CONTINUE
            );
        }
    }


    public Pattern getName() {
        return name;
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "GrandExchangeVertex(" + getName() + ", " + ", " + position.getX() + ", " +
            position.getY() + ", " + position.getPlane() + ')';
    }

    @Override
    public int getOpcode() {
        return 12;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(name.pattern());
        stream.writeInt(name.flags());
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.name = Pattern.compile(stream.readUTF(), stream.readInt());
        return true;
    }
}
