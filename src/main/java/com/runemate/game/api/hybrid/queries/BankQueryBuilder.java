package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.region.*;
import java.util.*;
import java.util.concurrent.*;

public class BankQueryBuilder extends LocatableEntityQueryBuilder<LocatableEntity, BankQueryBuilder> {
    @Override
    public BankQueryBuilder get() {
        return this;
    }

    @Override
    public Callable<List<? extends LocatableEntity>> getDefaultProvider() {
        return () -> Banks.getLoaded().asList();
    }
}
