package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.collections.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class GameObjectQueryBuilder
    extends LocatableEntityQueryBuilder<GameObject, GameObjectQueryBuilder> {
    protected GameObject.Type[] types;
    private int[] ids, models, appearance, mapFunctions, mapScenes;
    private Pattern[] names, actions;
    private Map<Color, Color> colorSubstitutions;
    private Color[] defaultColors;

    public GameObjectQueryBuilder names(final String... names) {
        return names(Regex.getPatternsForExactStrings(names).toArray(new Pattern[names.length]));
    }

    public GameObjectQueryBuilder names(final Pattern... names) {
        this.names = names;
        return get();
    }

    @Override
    public GameObjectQueryBuilder get() {
        return this;
    }

    @Override
    public Callable<List<? extends GameObject>> getDefaultProvider() {
        return () -> {
            if (on != null) {
                if (on.size() == 1) {
                    return GameObjects.getLoadedOn(on.iterator().next()).asList();
                }
                List<GameObject> objects = new ArrayList<>();
                for (Coordinate coordinate : on) {
                    objects.addAll(GameObjects.getLoadedOn(coordinate));
                }
                return objects;
            }
            if (within != null) {
                if (within.size() == 1) {
                    return GameObjects.getLoadedWithin(within.iterator().next()).asList();
                }
                List<GameObject> objects = new ArrayList<>();
                for (Area area : within) {
                    objects.addAll(GameObjects.getLoadedWithin(area));
                }
                return objects;
            }
            return GameObjects.getLoaded().asList();
        };
    }

    public GameObjectQueryBuilder ids(final int... ids) {
        this.ids = ids;
        return get();
    }

    public final GameObjectQueryBuilder mapsFunctions(final int... mapFunctions) {
        this.mapFunctions = mapFunctions;
        return get();
    }

    public final GameObjectQueryBuilder mapScenes(final int... mapScenes) {
        this.mapScenes = mapScenes;
        return get();
    }

    public GameObjectQueryBuilder actions(final String... actions) {
        return actions(
            Regex.getPatternsForExactStrings(actions).toArray(new Pattern[actions.length]));
    }

    public GameObjectQueryBuilder actions(final Pattern... actions) {
        this.actions = actions;
        return get();
    }

    public GameObjectQueryBuilder types(final Collection<GameObject.Type> types) {
        if (types == null) {
            return get();
        }
        return types(types.toArray(new GameObject.Type[0]));
    }

    public GameObjectQueryBuilder types(final GameObject.Type... types) {
        this.types = types;
        return get();
    }

    /**
     * Adds the requirement that the result has a model with one of the specified hashes
     */
    public final GameObjectQueryBuilder models(final int... modelHashes) {
        this.models = modelHashes;
        return get();
    }

    /**
     * Adds the requirement that the result has a model with one of the specified default colors
     */
    public final GameObjectQueryBuilder defaultColors(final Color... colors) {
        this.defaultColors = colors;
        return get();
    }

    public final GameObjectQueryBuilder colorSubstitutions(Color original, Color substitution) {
        return colorSubstitutions(new Pair<>(original, substitution));
    }

    public final GameObjectQueryBuilder colorSubstitutions(Pair<Color, Color> substitution) {
        Map<Color, Color> m = new HashMap<>();
        m.put(substitution.getLeft(), substitution.getRight());
        return colorSubstitutions(m);
    }

    public final GameObjectQueryBuilder colorSubstitutions(Map<Color, Color> substitutions) {
        this.colorSubstitutions = substitutions;
        return get();
    }

    public final GameObjectQueryBuilder appearance(final int... appearance) {
        this.appearance = appearance;
        return get();
    }

    @Override
    public boolean accepts(GameObject argument) {
        boolean condition;
        if (ids != null) {
            //Doesn't handle local state ids for now
            condition = false;
            int id = argument.getId();
            for (final int value : ids) {
                if (value == id) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (types != null) {
            condition = false;
            GameObject.Type type = argument.getType();
            for (final GameObject.Type value : types) {
                if (value == type) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        GameObjectDefinition definition = null;
        boolean hasDefinition = true;
        GameObjectDefinition localState = null;
        boolean hasLocalState = true;
        if (names != null) {
            condition = false;
            definition = argument.getDefinition();
            if (definition == null) {
                hasDefinition = false;
                hasLocalState = false;
            } else {
                String name = definition.getName();
                for (final Pattern value : this.names) {
                    if (value.matcher(name).find()) {
                        condition = true;
                        break;
                    }
                }
                if (!condition) {
                    localState = definition.getLocalState();
                    if (localState == null) {
                        hasLocalState = false;
                    } else {
                        name = localState.getName();
                        for (final Pattern value : this.names) {
                            if (value.matcher(name).find()) {
                                condition = true;
                                break;
                            }
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (mapScenes != null) {
            if (hasDefinition && definition == null) {
                definition = argument.getDefinition();
                if (definition == null) {
                    hasDefinition = false;
                    hasLocalState = false;
                }
            }
            condition = false;
            if (definition != null) {
                int scene = definition.getMapScene();
                for (final int scene2 : this.mapScenes) {
                    if (scene == scene2) {
                        condition = true;
                        break;
                    }
                }
                if (!condition) {
                    if (hasLocalState && localState == null) {
                        localState = definition.getLocalState();
                        if (localState == null) {
                            hasLocalState = false;
                        }
                    }
                    if (localState != null) {
                        scene = localState.getMapScene();
                        for (final int scene2 : this.mapScenes) {
                            if (scene == scene2) {
                                condition = true;
                                break;
                            }
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (mapFunctions != null) {
            if (hasDefinition && definition == null) {
                definition = argument.getDefinition();
                if (definition == null) {
                    hasDefinition = false;
                    hasLocalState = false;
                }
            }
            condition = false;
            if (definition != null) {
                int function = definition.getMapFunction();
                for (final int function2 : this.mapFunctions) {
                    if (function == function2) {
                        condition = true;
                        break;
                    }
                }
                if (!condition) {
                    if (hasLocalState && localState == null) {
                        localState = definition.getLocalState();
                        if (localState == null) {
                            hasLocalState = false;
                        }
                    }
                    if (localState != null) {
                        function = localState.getMapFunction();
                        for (final int function2 : this.mapFunctions) {
                            if (function == function2) {
                                condition = true;
                                break;
                            }
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (actions != null) {
            if (hasDefinition && definition == null) {
                definition = argument.getDefinition();
                if (definition == null) {
                    hasDefinition = false;
                    hasLocalState = false;
                }
            }
            condition = false;
            if (definition != null) {
                List<String> actions = definition.getActions();
                outer:
                for (final String string : actions) {
                    for (final Pattern pattern : this.actions) {
                        if (pattern.matcher(string).find()) {
                            condition = true;
                            break outer;
                        }
                    }
                }
                if (!condition) {
                    if (hasLocalState && localState == null) {
                        localState = definition.getLocalState();
                        if (localState == null) {
                            hasLocalState = false;
                        }
                    }
                    if (localState != null) {
                        actions = localState.getActions();
                        outer:
                        for (final String string : actions) {
                            for (final Pattern pattern : this.actions) {
                                if (pattern.matcher(string).find()) {
                                    condition = true;
                                    break outer;
                                }
                            }
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (appearance != null) {
            if (hasDefinition && definition == null) {
                definition = argument.getDefinition();
                if (definition == null) {
                    hasDefinition = false;
                    hasLocalState = false;
                }
            }
            condition = false;
            if (definition != null) {
                List<Integer> appearance = definition.getAppearance();
                outer:
                for (final int value : appearance) {
                    for (final int value2 : this.appearance) {
                        if (value == value2) {
                            condition = true;
                            break outer;
                        }
                    }
                }
                if (!condition) {
                    if (hasLocalState && localState == null) {
                        localState = definition.getLocalState();
                        if (localState == null) {
                            hasLocalState = false;
                        }
                    }
                    if (localState != null) {
                        appearance = localState.getAppearance();
                        outer:
                        for (final int value : appearance) {
                            for (final int value2 : this.appearance) {
                                if (value == value2) {
                                    condition = true;
                                    break outer;
                                }
                            }
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (colorSubstitutions != null) {
            if (hasDefinition && definition == null) {
                definition = argument.getDefinition();
                if (definition == null) {
                    hasDefinition = false;
                    hasLocalState = false;
                }
            }
            condition = false;
            if (definition != null) {
                Map<Color, Color> substitutions = definition.getColorSubstitutions();
                outer:
                for (Map.Entry<Color, Color> as : substitutions.entrySet()) {
                    for (Map.Entry<Color, Color> cs : colorSubstitutions.entrySet()) {
                        if (Objects.equals(as.getKey(), cs.getKey())
                            && Objects.equals(as.getValue(), cs.getValue())) {
                            condition = true;
                            break outer;
                        }
                    }
                }
                if (!condition) {
                    if (hasLocalState && localState == null) {
                        localState = definition.getLocalState();
                        if (localState == null) {
                            hasLocalState = false;
                        }
                    }
                    if (localState != null) {
                        substitutions = localState.getColorSubstitutions();
                        outer:
                        for (Map.Entry<Color, Color> as : substitutions.entrySet()) {
                            for (Map.Entry<Color, Color> cs : colorSubstitutions.entrySet()) {
                                if (Objects.equals(as.getKey(), cs.getKey()) &&
                                    Objects.equals(as.getValue(), cs.getValue())) {
                                    condition = true;
                                    break outer;
                                }
                            }
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        Model model = null;
        if (models != null) {
            condition = false;
            model = argument.getModel();
            if (model != null) {
                int modelHash = model.hashCode();
                for (final int hash : models) {
                    if (hash == modelHash) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (defaultColors != null) {
            condition = false;
            if (model == null) {
                model = argument.getModel();
            }
            if (model != null) {
                Set<Color> colors = model.getDefaultColors();
                outer:
                for (final Color dc : defaultColors) {
                    for (final Color c : colors) {
                        if (Objects.equals(c, dc)) {
                            condition = true;
                            break outer;
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        return super.accepts(argument);
    }
}
