package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import java.util.*;
import java.util.concurrent.*;
import lombok.*;

public abstract class LocatableEntityQueryBuilder<T extends LocatableEntity, QB extends QueryBuilder>
    extends InteractableQueryBuilder<T, QB, LocatableEntityQueryResults<T>> {
    protected Collection<Area> within;
    protected Collection<Coordinate> on, off;
    private Callable<Locatable> reachableFrom;
    private Boolean reachable, reachableSurroundings;
    private Double minimumVisibility, maximumVisibility;

    /**
     * Adds a new filter to the query for entities that are reachable.
     */
    public QB reachable() {
        return reachableFrom(Players::getLocal);
    }

    public QB reachableFrom(Callable<Locatable> callable) {
        reachable = true;
        reachableFrom = callable;
        return get();
    }

    public QB unreachable() {
        return unreachableFrom(Players::getLocal);
    }

    public QB unreachableFrom(Callable<Locatable> callable) {
        reachable = false;
        reachableFrom = callable;
        return get();
    }

    public QB reachableFrom(Locatable locatable) {
        return reachableFrom(() -> locatable);
    }

    public QB unreachableFrom(Locatable locatable) {
        return unreachableFrom(() -> locatable);
    }

    /**
     * Adds a new filter to the query for entities that have connecting tiles that are reachable.
     */
    public QB surroundingsReachable() {
        return surroundingsReachableFrom(Players.getLocal());
    }

    /**
     * Adds a new filter to the query for entities that have connecting tiles that are reachable.
     */
    public QB surroundingsReachableFrom(Locatable locatable) {
        return surroundingsReachableFrom(() -> locatable);
    }

    public QB surroundingsReachableFrom(Callable<Locatable> locatable) {
        reachableSurroundings = true;
        reachableFrom = locatable;
        return get();
    }

    public QB surroundingsUnreachable() {
        return surroundingsUnreachableFrom(Players.getLocal());
    }

    public QB surroundingsUnreachableFrom(Locatable locatable) {
        return surroundingsUnreachableFrom(() -> locatable);
    }

    public QB surroundingsUnreachableFrom(Callable<Locatable> locatable) {
        reachableSurroundings = false;
        reachableFrom = locatable;
        return get();
    }

    public QB visibility(double minimum) {
        minimumVisibility = minimum;
        return get();
    }

    public QB visibility(double minimum, double maximum) {
        minimumVisibility = minimum;
        maximumVisibility = maximum;
        return get();
    }

    public final QB on(final Coordinate... positions) {
        return on(Arrays.asList(positions));
    }

    public final QB on(final Collection<Coordinate> positions) {
        this.on = positions;
        return get();
    }

    public final QB off(final Coordinate... positions) {
        return off(Arrays.asList(positions));
    }

    public final QB off(final Collection<Coordinate> positions) {
        this.off = positions;
        return get();
    }

    /**
     * Applies a filter to the query that accepts provided LocatableEntity's that have a Locatable#getPosition() that lies within one of the provided areas.
     *
     * @param areas a variable amount of non-null Areas
     * @return itself
     */
    public final QB within(@NonNull final Area... areas) {
        return within(Arrays.asList(areas));
    }

    /**
     * Applies a filter to the query that accepts provided LocatableEntity's that have a Locatable#getPosition() that lies within one of the provided areas.
     *
     * @param areas a Collection of non-null Areas
     * @return itself
     */
    public final QB within(@NonNull Collection<Area> areas) {
        this.within = areas;
        return get();
    }

    @Override
    protected LocatableEntityQueryResults<T> results(
        Collection<? extends T> entries,
        ConcurrentMap<String, Object> cache
    ) {
        return new LocatableEntityQueryResults<>(entries, cache);
    }

    @Override
    public boolean accepts(T argument) {
        boolean condition;
        Area.Rectangular area = null;
        if (on != null) {
            condition = false;
            if (area == null) {
                area = argument.getArea(getRegionBase());
            }
            if (area != null) {
                for (final Coordinate coordinate : on) {
                    if (area.contains(coordinate)) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (off != null) {
            condition = true;
            if (area == null) {
                area = argument.getArea(getRegionBase());
            }
            if (area != null) {
                for (final Coordinate coordinate : off) {
                    if (area.contains(coordinate)) {
                        condition = false;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        Coordinate position = null;
        if (within != null) {
            position = argument.getPosition(getRegionBase());
            condition = false;
            if (position != null) {
                for (final Area area_ : within) {
                    if (area_.contains(position)) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (minimumVisibility != null) {
            condition = false;
            double visibility = argument.getVisibility();
            if (visibility >= minimumVisibility) {
                if (maximumVisibility == null || visibility <= maximumVisibility) {
                    condition = true;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (super.accepts(argument)) {
            if (reachable != null) {
                if (position == null) {
                    position = argument.getPosition(getRegionBase());
                }
                condition = position != null &&
                    Objects.equals(
                        cachingGetReachableFrom(reachableFrom).contains(position),
                        reachable
                    );
                if (!condition) {
                    return false;
                }
            }
            if (reachableSurroundings != null) {
                condition = false;
                if (area == null) {
                    area = argument.getArea(getRegionBase());
                }
                if (area != null) {
                    boolean reached = false;
                    List<Coordinate> surroundings = area.getSurroundingCoordinates();
                    Collection<Coordinate> reachable = cachingGetReachableFrom(reachableFrom);
                    for (Coordinate surrounding : surroundings) {
                        if (reachable.contains(surrounding)) {
                            reached = true;
                            break;
                        }
                    }
                    condition = Objects.equals(reachableSurroundings, reached);
                }
                return condition;
            }
            return true;
        } else {
            return false;
        }
    }

    private Coordinate getRegionBase() {
        ConcurrentMap<String, Object> cache = getCache();
        Coordinate region_base;
        synchronized (LOCK) {
            region_base = (Coordinate) cache.get("region_base");
            if (region_base == null) {
                region_base = Region.getBase();
                if (region_base != null) {
                    cache.put("region_base", region_base);
                }
            }
        }
        return region_base;
    }

    @SneakyThrows(Exception.class)
    private Collection<Coordinate> cachingGetReachableFrom(Callable<Locatable> callable) {
        ConcurrentMap<String, Object> cache = getCache();
        Collection<Coordinate> reachable;
        synchronized (LOCK) {
            reachable = (Collection<Coordinate>) cache.get("reachable");
            if (reachable == null) {
                Locatable locatable = callable.call();
                if (locatable != null) {
                    Coordinate coordinate = locatable.getPosition();
                    if (coordinate != null) {
                        reachable = coordinate.getReachableCoordinates();
                        cache.put("reachable", reachable);
                    } else {
                        cache.put("reachable", reachable = Collections.emptyList());
                    }
                } else {
                    cache.put("reachable", reachable = Collections.emptyList());
                }
            }
        }
        return reachable;
    }
}