package com.runemate.game.api.hybrid.queries.results;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.awt.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import lombok.extern.log4j.*;

@Log4j2
public abstract class InteractableQueryResults<T extends Interactable, QR extends InteractableQueryResults>
    extends QueryResults<T, QR> {
    public InteractableQueryResults(final Collection<? extends T> results) {
        super(results);
    }

    public InteractableQueryResults(
        final Collection<? extends T> results,
        ConcurrentMap<String, Object> cache
    ) {
        super(results, cache);
    }

    public final QR sortByDistanceFromMouse() {
        return sortByDistanceFrom(Mouse.getPosition());
    }

    public final QR sortByDistanceFrom(Point point) {
        return sortByDistanceFrom(point, Distance.Algorithm.MANHATTAN);
    }

    public final QR sortByDistanceFrom(Point point, Distance.Algorithm algorithm) {
        Map<T, Point.Double> lookup = new HashMap<>();
        return sort(Comparator.comparingDouble((ToDoubleFunction<? super T>) key -> {
            Point.Double value = lookup.get(key);
            if (value == null) {
                Rectangle bounds = null;
                if (key instanceof SpriteItem) {
                    bounds = ((SpriteItem) key).getBounds();
                } else if (key instanceof InterfaceComponent) {
                    bounds = ((InterfaceComponent) key).getBounds();
                } else if (key instanceof Modeled) {
                    Model model = ((Modeled) key).getModel();
                    if (model != null) {
                        Polygon convexHull = model.projectConvexHull();
                        if (convexHull != null) {
                            bounds = convexHull.getBounds();
                        }
                    }
                } else if (key instanceof Locatable) {
                    bounds = ((Locatable) key).getArea().getCenter().getBounds().getBounds();
                } else {
                    log.warn("Cannot sort by the distance between {} and {} because type isn't supported.", key, point);
                }
                if (bounds == null) {
                    return Double.NaN;
                }
                value = new Point.Double(bounds.getCenterX(), bounds.getCenterY());
                lookup.put(key, value);
            }
            return algorithm.calculate(point.x, point.y, value.x, value.y);
        }));
    }
}
