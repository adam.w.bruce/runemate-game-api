package com.runemate.game.api.hybrid.structures;

public class Matrix4f {
    public final float[] m = new float[16];

    public Matrix4f() {
        this.m[0] = 1.0F;
        this.m[1] = 0.0F;
        this.m[2] = 0.0F;
        this.m[3] = 0.0F;
        this.m[4] = 0.0F;
        this.m[5] = 1.0F;
        this.m[6] = 0.0F;
        this.m[7] = 0.0F;
        this.m[8] = 0.0F;
        this.m[9] = 0.0F;
        this.m[10] = 1.0F;
        this.m[11] = 0.0F;
        this.m[12] = 0.0F;
        this.m[13] = 0.0F;
        this.m[14] = 0.0F;
        this.m[15] = 1.0F;
    }

    public Matrix4f copyFrom(final Matrix4x3f matrix4X3F) {
        this.m[0] = matrix4X3F.m11;
        this.m[1] = matrix4X3F.m12;
        this.m[2] = matrix4X3F.m13;
        this.m[3] = 0.0F;
        this.m[4] = matrix4X3F.m21;
        this.m[5] = matrix4X3F.m22;
        this.m[6] = matrix4X3F.m23;
        this.m[7] = 0.0F;
        this.m[8] = matrix4X3F.m31;
        this.m[9] = matrix4X3F.m32;
        this.m[10] = matrix4X3F.m33;
        this.m[11] = 0.0F;
        this.m[12] = matrix4X3F.m41;
        this.m[13] = matrix4X3F.m42;
        this.m[14] = matrix4X3F.m43;
        this.m[15] = 1.0F;
        return this;
    }

    public boolean isIdentity() {
        return this.m[0] == 1.0F && this.m[1] == 0.0F && this.m[2] == 0.0F
            && this.m[3] == 0.0F && this.m[4] == 0.0F && this.m[5] == 1.0F
            && this.m[6] == 0.0F && this.m[7] == 0.0F && this.m[8] == 0.0F
            && this.m[9] == 0.0F && this.m[10] == 1.0F && this.m[11] == 0.0F
            && this.m[12] == 0.0F && this.m[13] == 0.0F && this.m[14] == 0.0F
            && this.m[15] == 1.0F;
    }

    public Matrix4f multiply(Matrix4f mult) {
        float new00 = this.m[0] * mult.m[0] + this.m[1] * mult.m[4] + this.m[2] * mult.m[8] +
            this.m[3] * mult.m[12];
        float new01 = this.m[0] * mult.m[1] + this.m[1] * mult.m[5] + this.m[2] * mult.m[9] +
            this.m[3] * mult.m[13];
        float new02 = this.m[0] * mult.m[2] + this.m[1] * mult.m[6] + this.m[2] * mult.m[10] +
            this.m[3] * mult.m[14];
        float new03 = this.m[0] * mult.m[3] + this.m[1] * mult.m[7] + this.m[2] * mult.m[11] +
            this.m[3] * mult.m[15];
        float new10 = this.m[4] * mult.m[0] + this.m[5] * mult.m[4] + this.m[6] * mult.m[8] +
            this.m[7] * mult.m[12];
        float new11 = this.m[4] * mult.m[1] + this.m[5] * mult.m[5] + this.m[6] * mult.m[9] +
            this.m[7] * mult.m[13];
        float new12 = this.m[4] * mult.m[2] + this.m[5] * mult.m[6] + this.m[6] * mult.m[10] +
            this.m[7] * mult.m[14];
        float new13 = this.m[4] * mult.m[3] + this.m[5] * mult.m[7] + this.m[6] * mult.m[11] +
            this.m[7] * mult.m[15];
        float new20 = this.m[8] * mult.m[0] + this.m[9] * mult.m[4] + this.m[10] * mult.m[8] +
            this.m[11] * mult.m[12];
        float new21 = this.m[8] * mult.m[1] + this.m[9] * mult.m[5] + this.m[10] * mult.m[9] +
            this.m[11] * mult.m[13];
        float new22 = this.m[8] * mult.m[2] + this.m[9] * mult.m[6] + this.m[10] * mult.m[10] +
            this.m[11] * mult.m[14];
        float new23 = this.m[8] * mult.m[3] + this.m[9] * mult.m[7] + this.m[10] * mult.m[11] +
            this.m[11] * mult.m[15];
        float new30 = this.m[12] * mult.m[0] + this.m[13] * mult.m[4] + this.m[14] * mult.m[8] +
            this.m[15] * mult.m[12];
        float new31 = this.m[12] * mult.m[1] + this.m[13] * mult.m[5] + this.m[14] * mult.m[9] +
            this.m[15] * mult.m[13];
        float new32 = this.m[12] * mult.m[2] + this.m[13] * mult.m[6] + this.m[14] * mult.m[10] +
            this.m[15] * mult.m[14];
        float new33 = this.m[12] * mult.m[3] + this.m[13] * mult.m[7] + this.m[14] * mult.m[11] +
            this.m[15] * mult.m[15];
        this.m[0] = new00;
        this.m[1] = new01;
        this.m[2] = new02;
        this.m[3] = new03;
        this.m[4] = new10;
        this.m[5] = new11;
        this.m[6] = new12;
        this.m[7] = new13;
        this.m[8] = new20;
        this.m[9] = new21;
        this.m[10] = new22;
        this.m[11] = new23;
        this.m[12] = new30;
        this.m[13] = new31;
        this.m[14] = new32;
        this.m[15] = new33;
        return this;
    }

    public void loadPerspective(float near, float far, float xFieldOfView, float yFieldOfView) {
        //TODO this and loadFrustum have some weird positive/negatives, check later
        float right = (float) (StrictMath.tan(xFieldOfView / 2.0F) * near);
        float top = (float) (StrictMath.tan(yFieldOfView / 2.0F) * near);
        this.loadFrustum(-right, right, -top, top, near, far);
    }

    /**
     * Loads values as a perspective projection matrix (see loadPerspective).
     *
     * @param left   left vertical clipping plane
     * @param right  right vertical clipping plane
     * @param bottom bottom horizontal clipping plane
     * @param top    top horizontal clipping plane
     * @param near   near clipping plane. Must be positive
     * @param far    far clipping plane. Must be positive
     */
    private void loadFrustum(
        float left, float right, float bottom, float top, float near,
        float far
    ) {
        this.m[0] = 2.0F * near / (right - left);
        this.m[1] = 0.0F;
        this.m[2] = 0.0F;
        this.m[3] = 0.0F;
        this.m[4] = 0.0F;
        this.m[5] = 2.0F * near / (top - bottom);
        this.m[6] = 0.0F;
        this.m[7] = 0.0F;
        this.m[8] = (right + left) / (right - left);
        this.m[9] = (top + bottom) / (top - bottom);
        this.m[10] = (far + near) / (far - near);
        this.m[11] = 1.0F;
        this.m[12] = 0.0F;
        this.m[13] = 0.0F;
        this.m[14] = -(2.0F * far * near) / (far - near);
        this.m[15] = 0.0F;
    }
}
