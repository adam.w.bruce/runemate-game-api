package com.runemate.game.api.hybrid.util;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.util.*;

public class CachingDistanceComparator implements Comparator<Locatable> {
    private final HashMap<Locatable, Double> cached = new HashMap<>();
    private final HashMap<String, Object> cache = new HashMap<>();
    private final Locatable from;
    private final Distance.Algorithm algorithm;

    public CachingDistanceComparator(Locatable from, Distance.Algorithm algorithm) {
        //Hack to avoid recalling getArea and having to actually query the game
        this.from = from != null ? from.getArea() : null;
        this.algorithm = algorithm;
    }

    @Override
    public int compare(Locatable one, Locatable two) {
        Double d1 = cached.get(one);
        if (d1 == null) {
            cached.put(one, d1 = Distance.between(from, one, algorithm, cache));
        }
        Double d2 = cached.get(two);
        if (d2 == null) {
            cached.put(two, d2 = Distance.between(from, two, algorithm, cache));
        }
        return Double.compare(d1, d2);
    }
}
