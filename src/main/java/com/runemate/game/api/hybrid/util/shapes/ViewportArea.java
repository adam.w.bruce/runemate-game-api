package com.runemate.game.api.hybrid.util.shapes;

import com.runemate.game.api.hybrid.projection.*;
import java.awt.*;
import java.awt.geom.*;

public class ViewportArea extends Area {

    public ViewportArea(final Shape s) {
        super(s);
    }

    public Area getVisibleArea() {
        return getVisibleArea(Projection.getViewport());
    }

    public Area getVisibleArea(Shape viewport) {
        if (viewport == null) {
            return null;
        }
        final var visible = new ViewportArea(this);
        final var other = new ViewportArea(viewport);
        visible.intersect(other);
        return visible;
    }

    public double getVisibility() {
        return getVisibility(Projection.getViewport());
    }

    public double getVisibility(Shape viewport) {
        if (viewport == null) {
            return 0;
        }
        final var visible = new ViewportArea(this);
        final var other = new ViewportArea(viewport);
        visible.intersect(other);
        return (visible.getArea() / getArea()) * 100;
    }

    private double getArea() {
        // Calculate the area of the area
        double areaValue = 0.0;
        PathIterator pathIterator = getPathIterator(null);
        double[] coords = new double[6];
        double startX = 0.0;
        double startY = 0.0;
        while (!pathIterator.isDone()) {
            int type = pathIterator.currentSegment(coords);
            switch (type) {
                case PathIterator.SEG_MOVETO:
                    startX = coords[0];
                    startY = coords[1];
                    break;
                case PathIterator.SEG_LINETO:
                    double endX = coords[0];
                    double endY = coords[1];
                    areaValue += startX * endY - endX * startY;
                    startX = endX;
                    startY = endY;
                    break;
                case PathIterator.SEG_CLOSE:
                    break;
            }
            pathIterator.next();
        }
        areaValue /= 2.0;
        return Math.abs(areaValue);
    }
}
