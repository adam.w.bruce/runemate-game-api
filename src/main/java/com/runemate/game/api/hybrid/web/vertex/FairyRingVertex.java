package com.runemate.game.api.hybrid.web.vertex;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.input.direct.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.api.hybrid.web.*;
import com.runemate.game.api.script.*;
import java.util.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
@ToString
@RequiredArgsConstructor
public class FairyRingVertex implements Vertex {

    @NonNull
    private final Coordinate position;
    @NonNull
    private final FairyRing ring;

    @NonNull
    @Override
    public Coordinate getPosition() {
        return position;
    }

    @Nullable
    @Override
    public Area.Rectangular getArea() {
        return new Area.Rectangular(position.derive(-1, -1), position.derive(1, 1));
    }

    @Override
    public boolean step(final Map<String, Object> cache) {
        final var local = (Player) cache.get(WebPath.AVATAR);
        final var localPos = (Coordinate) cache.get(WebPath.AVATAR_POS);


        if (!FairyRing.isConfigurationOpen()) {
            final var object = getObject();
            if (object == null) {
                log.warn("Failed to resolve target object for {}", this);
                return false;
            }

            if (object.getVisibility() <= 40) {
                Camera.concurrentlyTurnTo(object);
            }

            if (ring.isPreviousDestination()) {
                log.debug("Using \"Last-destination\" option for {}", ring);
                var action = "Last-destination (" + ring.name() + ")";
                if ((boolean) cache.get(WebPath.DIRECT_INPUT)) {
                    var ma = MenuAction.forGameObject(object, action);
                    if (ma != null) {
                        DirectInput.send(ma);
                        return Execution.delayUntil(() -> local.getAnimationId() != -1, 1200)
                            && Execution.delayWhile(() -> localPos.equals(local.getPosition()), () -> local.getAnimationId() != -1, 2400);
                    }
                }

                return object.interact(action)
                    && Execution.delayUntil(() -> local.getAnimationId() != -1, 1200)
                    && Execution.delayWhile(() -> localPos.equals(local.getPosition()), () -> local.getAnimationId() != -1, 2400);
            }

            if ((boolean) cache.get(WebPath.DIRECT_INPUT)) {
                var ma = MenuAction.forGameObject(object, "Configure");
                if (ma != null) {
                    DirectInput.send(ma);
                    return Execution.delayUntil(FairyRing::isConfigurationOpen, local::isMoving, 2400);
                }
            }

            return object.interact("Configure")
                && Execution.delayUntil(FairyRing::isConfigurationOpen, local::isMoving, 2400);
        }

        return ring.select()
            && Execution.delayUntil(() -> local.getAnimationId() != -1, 2400)
            && Execution.delayWhile(() -> localPos.equals(local.getPosition()), () -> local.getAnimationId() != -1, 2400);
    }

    @Nullable
    public GameObject getObject() {
        return FairyRing.getNearestObject();
    }

    @Override
    public ScanResult scan(final Map<String, Object> cache) {
        final var object = getObject();
        if (object == null) {
            return new ScanResult(null, ScanAction.CONTINUE);
        }

        final var player = (Player) cache.get(WebPath.AVATAR);
        if (player == null || Distance.between(player, object) > 6) {
            return new ScanResult(null, ScanAction.CONTINUE);
        }

        return new ScanResult(this, ScanAction.STOP);
    }
}
