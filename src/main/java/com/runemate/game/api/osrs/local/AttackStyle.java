package com.runemate.game.api.osrs.local;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.local.*;
import java.util.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Getter
@Log4j2
@ToString
public enum AttackStyle {
    ACCURATE("Accurate", Skill.ATTACK),
    AGGRESSIVE("Aggressive", Skill.STRENGTH),
    DEFENSIVE("Defensive", Skill.DEFENCE),
    CONTROLLED("Controlled", Skill.ATTACK, Skill.STRENGTH, Skill.DEFENCE),
    RANGING("Ranging", Skill.RANGED),
    LONGRANGE("Longrange", Skill.RANGED, Skill.DEFENCE),
    CASTING("Casting", Skill.MAGIC),
    DEFENSIVE_CASTING("Defensive Casting", Skill.MAGIC, Skill.DEFENCE),
    OTHER("Other");

    private static final int WEAPON_STYLES_INDEX_ENUM_ID = 3908;
    private static final int ATTACK_STYLE_NAME_PARAM_ID = 1407;

    private final String name;
    private final Skill[] skills;

    AttackStyle(String name, Skill... skills) {
        this.name = name;
        this.skills = skills;
    }

    @Nullable
    public static AttackStyle getCurrent() {
        final AttackStyle[] available = getAvailable();
        // Casting styles are always at index 4, but the defensive casting attack style is at index 5 in the definitions
        final int attackStyle = VarpID.ATTACK_STYLE.getValue() + VarbitID.DEFENSIVE_CASTING_MODE.getValue();
        if (attackStyle > -1 && attackStyle < available.length) {
            return available[attackStyle];
        }
        return null;
    }

    public static AttackStyle[] getAvailable() {
        return get(VarbitID.EQUIPPED_WEAPON_TYPE.getValue());
    }

    @NotNull
    public static AttackStyle[] get(int weaponType) {
        final EnumDefinition weaponStylesIndex = EnumDefinitions.load(WEAPON_STYLES_INDEX_ENUM_ID);
        if (weaponStylesIndex == null) {
            log.error("Failed to load weapon styles index enum definition");
            return new AttackStyle[6];
        }

        final EnumDefinition weaponStyles = EnumDefinitions.load(weaponStylesIndex.getInt(weaponType));
        if (weaponStyles == null) {
            log.error("Failed to load weapon styles enum definition");
            return new AttackStyle[6];
        }

        final Integer[] attackStyleIds = Arrays.stream(weaponStyles.getValues())
            .map(id -> (Integer) id)
            .toArray(Integer[]::new);

        AttackStyle[] attackStyles = new AttackStyle[attackStyleIds.length];
        for (int i = 0; i < attackStyleIds.length; i++) {
            final Struct struct = Structs.load(attackStyleIds[i]);
            if (struct == null) {
                log.error("Failed to load attack style struct for id {}", attackStyleIds[i]);
                continue;
            }

            final String styleName = (String) struct.getParams().get(ATTACK_STYLE_NAME_PARAM_ID);

            AttackStyle style = AttackStyle.valueOf(styleName.toUpperCase());
            if (style == OTHER) {
                // Other is used as a filler
                continue;
            }

            if (i == 5 && style == DEFENSIVE) {
                // Defensive on index 5 is always defensive casting
                style = DEFENSIVE_CASTING;
            }

            attackStyles[i] = style;
        }

        return attackStyles;
    }

}
