package com.runemate.game.api.osrs.local;

import com.runemate.game.api.hybrid.local.*;

/**
 * @deprecated this was removed from OSRS <a href="https://oldschool.runescape.wiki/w/Update:Children_of_the_Sun">in an update</a>.
 */
@Deprecated
public enum KourendHouseFavour {
    HOSIDIUS(4895, 4972),
    SHAYZIEN(4894),
    ARCEUUS(4896),
    LOVAKANGJ(4898, 4973),
    PISCARILIUS(4899);
    private final int percentageVarbit;
    private final int cantDecreaseVarbit;

    KourendHouseFavour(int percentageVarbit) {
        this(percentageVarbit, -1);
    }

    KourendHouseFavour(int percentageVarbit, int cantDecreaseVarbit) {
        this.percentageVarbit = percentageVarbit;
        this.cantDecreaseVarbit = cantDecreaseVarbit;
    }

    /**
     * Gets the current percent of favour for the house in Kourend
     *
     * @return a double ranging from 0 to 100.0 if the varbit can be loaded, decimal available. Otherwise -1.
     */
    public double getPercent() {
        Varbit varbit = Varbits.load(this.percentageVarbit);
        return varbit != null ? varbit.getValue() / 10D : -1;
    }

    /*public boolean canDecrease() {
        if (cantDecreaseVarbit == -1) {
            return true;
        }
        Varbit varbit = Varbits.load(this.cantDecreaseVarbit);
        return varbit == null || varbit.getValue() == 0;
    }*/
}
