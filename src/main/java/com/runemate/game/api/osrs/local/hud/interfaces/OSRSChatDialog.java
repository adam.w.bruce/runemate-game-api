package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.queries.results.*;
import java.awt.*;
import java.util.List;
import java.util.*;

public final class OSRSChatDialog {
    private static final Color TITLE_COLOR = new Color(128, 0, 0);
    private static final Color CLICK_TO_CONTINUE_COLOR = new Color(0, 0, 128);
    private static final int[] CONTAINERS = { 11, 193, 217, 219, 229, 231, 233, 633 };

    private OSRSChatDialog() {
    }

    public static boolean isOpen() {
        return !Interfaces.newQuery().containers(CONTAINERS)
            .types(InterfaceComponent.Type.CONTAINER)
            .visible()
            .results()
            .isEmpty();
    }

    public static String getTitle() {
        InterfaceComponent component = Interfaces.newQuery()
            .containers(217, 231)
            .grandchildren(false)
            .visible()
            .textColors(TITLE_COLOR)
            .types(InterfaceComponent.Type.LABEL)
            .results()
            .first();

        if (component == null) {
            component = Interfaces.newQuery()
                .containers(219)
                .visible()
                .textColors(TITLE_COLOR)
                .types(InterfaceComponent.Type.LABEL)
                .results()
                .first();
        }
        return component != null ? component.getText() : null;
    }

    public static List<ChatDialog.Option> getOptions() {
        List<ChatDialog.Option> options = new ArrayList<>();
        InterfaceComponentQueryResults res = Interfaces.newQuery().containers(219).types(InterfaceComponent.Type.LABEL).results();
        for (InterfaceComponent ic : res) {
            int id = ic.getIndex();
            if (id > 0 && !ic.getText().isEmpty()) {
                options.add(new ChatDialog.Option(ic, id));
            }
        }
        return options;
    }

    private static InterfaceComponent getContainedItemComponent() {
        return Interfaces.newQuery().containers(193)
            .types(InterfaceComponent.Type.MODEL)
            .grandchildren(false)
            .visible()
            .results().first();
    }

    public static ItemDefinition getContainedItem() {
        InterfaceComponent component = getContainedItemComponent();
        return component != null ? component.getContainedItem() : null;
    }

    public static int getContainedItemId() {
        InterfaceComponent component = getContainedItemComponent();
        return component != null ? component.getContainedItemId() : -1;
    }

    public static int getContainedItemQuantity() {
        InterfaceComponent component = getContainedItemComponent();
        return component != null ? component.getContainedItemQuantity() : -1;
    }

    private static InterfaceComponent getProjectedNpcComponent() {
        return Interfaces.newQuery().containers(231)
            .types(InterfaceComponent.Type.MODEL)
            .grandchildren(false)
            .visible()
            .results().first();
    }

    public static NpcDefinition getProjectedNpc() {
        InterfaceComponent component = getProjectedNpcComponent();
        return component != null ? component.getProjectedNpc() : null;
    }

    public static int getProjectedNpcAnimationId() {
        InterfaceComponent component = getProjectedNpcComponent();
        return component != null ? component.getProjectedEntityAnimationId() : -1;
    }

    public static String getText() {
        InterfaceComponent component = Interfaces.newQuery()
            .containers(CONTAINERS)
            .types(InterfaceComponent.Type.LABEL)
            .textColors(Color.BLACK)
            .grandchildren(false)
            .visible()
            .results()
            .first();
        return component != null ? component.getText() : null;
    }


    public static InterfaceComponent getContinueButton() {
        //grandchildren must be true for at least 193
        return Interfaces.newQuery()
            .containers(CONTAINERS)
            .grandchildren(true)
            .texts("Click here to continue", "Click to continue", "Please wait...")
            .textColors(Color.BLUE, Color.WHITE, CLICK_TO_CONTINUE_COLOR)
            .types(InterfaceComponent.Type.LABEL)
            .visible()
            .results()
            .first();
    }
}
