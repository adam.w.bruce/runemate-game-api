package com.runemate.game.api.osrs.location;

import lombok.*;
import lombok.extern.log4j.Log4j2;

@Getter
@Log4j2
public enum WorldRegion {

    AMERICA("US", "USA"),
    UNITED_KINGDOM("GB", "GBR"),
    AUSTRALIA("AU", "AUS"),
    GERMANY("DE", "DEU");

    /**
     * ISO-3166-1 alpha-2 country code
     */
    private final String alpha2;
    /**
     * ISO-3166-1 alpha-3 country code
     */
    private final String alpha3;

    WorldRegion(final String alpha2, final String alpha3) {
        this.alpha2 = alpha2;
        this.alpha3 = alpha3;
    }

    public static WorldRegion valueOf(int location) {
        switch (location) {
            case 0:
                return AMERICA;
            case 1:
                return UNITED_KINGDOM;
            case 3:
                return AUSTRALIA;
            case 7:
                return GERMANY;
            default:
                log.warn("Failed to resolve region from id {}", location);
                return null;
        }
    }
}
