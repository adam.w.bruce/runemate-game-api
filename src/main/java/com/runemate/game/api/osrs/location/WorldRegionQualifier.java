package com.runemate.game.api.osrs.location;

public enum WorldRegionQualifier {
    EAST,
    WEST
}
