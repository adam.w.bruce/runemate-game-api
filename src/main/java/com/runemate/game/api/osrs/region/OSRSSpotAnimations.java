package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.entities.*;
import java.rmi.*;
import java.util.*;
import java.util.function.*;

public class OSRSSpotAnimations {

    public static LocatableEntityQueryResults<SpotAnimation> getLoaded(Predicate<SpotAnimation> filter) {
        OpenWorldView view = OpenWorldView.getTopLevelWorldView();
        final ArrayList<SpotAnimation> spotAnimationEntities = new ArrayList<>(4);
        try {
            for (OpenSpotAnimation model : view.getLoadedSpotAnimations()) {
                SpotAnimation animation = new OSRSSpotAnimation(model);
                if (filter == null || filter.test(animation)) {
                    spotAnimationEntities.add(animation);
                }
            }
        } catch (RemoteException e) {
            return new LocatableEntityQueryResults<>(new ArrayList<>());
        }

        return new LocatableEntityQueryResults<>(spotAnimationEntities);
    }
}
