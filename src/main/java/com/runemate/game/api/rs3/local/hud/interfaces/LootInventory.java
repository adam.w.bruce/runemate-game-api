package com.runemate.game.api.rs3.local.hud.interfaces;

import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import java.awt.event.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;

@Deprecated
public final class LootInventory {

    private LootInventory() {
    }

    /**
     * Checks if any items match the given {@link Predicate filter}
     *
     * @param filter the predicate to check the items against
     * @return true if an item matches the filter
     */
    public static boolean contains(Predicate<SpriteItem> filter) {
        return false;
    }

    /**
     * Checks if any items match the given id
     *
     * @param id the id to check the items against
     * @return true if an item matches the id
     */
    public static boolean contains(int id) {
        return false;
    }

    /**
     * Checks if any items match the given name
     *
     * @param name the name to check the items against
     * @return true if an item matches the name
     */
    public static boolean contains(String name) {
        return false;
    }

    /**
     * Checks if any items match the given name
     *
     * @param name the name to check the items against
     * @return true if an item matches the name
     */
    public static boolean contains(Pattern name) {
        return false;
    }

    /**
     * Checks if the supplied {@link Predicate filter} matches at least one item
     *
     * @param predicate the predicate to check the items against
     * @return true if the predicate matches an item
     */
    public static boolean containsAllOf(final Predicate<SpriteItem> predicate) {
        return false;
    }

    /**
     * Checks if all of the supplied {@link Predicate filter}s match at least one item each
     *
     * @param filters the predicates to check the items against
     * @return true if all of the predicates have a match
     */
    @SafeVarargs
    public static boolean containsAllOf(final Predicate<SpriteItem>... filters) {
        return false;
    }

    /**
     * Checks if all of the supplied ids match at least one item each
     *
     * @param ids the ids to check the items against
     * @return true if all of the ids have a match
     */
    public static boolean containsAllOf(final int... ids) {
        return false;
    }

    /**
     * Checks if all of the supplied names match at least one item each
     *
     * @param names the names to check the items against
     * @return true if all of the names have a match
     */
    public static boolean containsAllOf(final String... names) {
        return false;
    }

    /**
     * Checks if all of the supplied names match at least one item each
     *
     * @param names the names to check the items against
     * @return true if all of the names have a match
     */
    public static boolean containsAllOf(final Pattern... names) {
        return false;
    }

    /**
     * Checks if any items don't match the given {@link Predicate filter}
     *
     * @param filter the predicate to check the items against
     * @return true if at least one item doesn't match the filter
     */
    public static boolean containsAnyExcept(final Predicate<SpriteItem> filter) {
        return false;
    }

    /**
     * Checks if any items don't match the given {@link Predicate filter}s
     *
     * @param filters the predicates to check the items against
     * @return true if at least one item doesn't match the filters
     */
    @SafeVarargs
    public static boolean containsAnyExcept(final Predicate<SpriteItem>... filters) {
        return false;
    }

    /**
     * Checks if any items don't match the given ids
     *
     * @param ids the ids to check the items against
     * @return true if at least one item doesn't match the ids
     */
    public static boolean containsAnyExcept(final int... ids) {
        return false;
    }

    /**
     * Checks if any items don't match the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item doesn't match the names
     */
    public static boolean containsAnyExcept(final String... names) {
        return false;
    }

    /**
     * Checks if any items don't match the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item doesn't match the names
     */
    public static boolean containsAnyExcept(final Pattern... names) {
        return false;
    }

    /**
     * Checks if any item matches the given {@link Predicate filter}
     *
     * @param filter the filter to check the items against
     * @return true if at least one item matches the filter
     */
    public static boolean containsAnyOf(final Predicate<SpriteItem> filter) {
        return false;
    }

    /**
     * Checks if any item matches the given {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return true if at least one item matches a filter
     */
    @SafeVarargs
    public static boolean containsAnyOf(final Predicate<SpriteItem>... filters) {
        return false;
    }

    /**
     * Checks if any item matches the given ids
     *
     * @param ids the ids to check the items against
     * @return true if at least one item matches an id
     */
    public static boolean containsAnyOf(final int... ids) {
        return false;
    }

    /**
     * Checks if any item matches the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item matches a name
     */
    public static boolean containsAnyOf(final String... names) {
        return false;
    }

    /**
     * Checks if any item matches the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item matches a name
     */
    public static boolean containsAnyOf(final Pattern... names) {
        return false;
    }

    /**
     * Checks if all of the items match the given {@link Predicate filter}
     *
     * @param filter the filter to check the items against
     * @return true if all items match the filter
     */
    public static boolean containsOnly(final Predicate<SpriteItem> filter) {
        return false;
    }

    /**
     * Checks if all of the items match the given {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return true if all items match at least one filter each
     */
    @SafeVarargs
    public static boolean containsOnly(final Predicate<SpriteItem>... filters) {
        return false;
    }

    /**
     * Checks if all of the items match the given names
     *
     * @param names the filters to check the items against
     * @return true if all items match at least one name each
     */
    public static boolean containsOnly(final String... names) {
        return false;
    }

    /**
     * Checks if all of the items match the given names
     *
     * @param names the filters to check the items against
     * @return true if all items match at least one name each
     */
    public static boolean containsOnly(final Pattern... names) {
        return false;
    }

    /**
     * Gets the total quantity of items
     *
     * @return the total quantity of items
     */
    public static int getQuantity() {
        return 0;
    }

    /**
     * Gets the total quantity of items matching the filter
     *
     * @param filter the filter to check the items against
     * @return the total quantity of items matching the filter
     */
    public static int getQuantity(final Predicate<SpriteItem> filter) {
        return 0;
    }

    /**
     * Gets the total quantity of items matching the {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return the total quantity of items matching the filters
     */
    @SafeVarargs
    public static int getQuantity(final Predicate<SpriteItem>... filters) {
        return 0;
    }

    /**
     * Gets the total quantity of items matching the ids
     *
     * @param ids the ids to check the items against
     * @return the total quantity of items matching the ids
     */
    public static int getQuantity(final int... ids) {
        return 0;
    }

    /**
     * Gets the total quantity of items matching the names
     *
     * @param names the ids to check the items against
     * @return the total quantity of items matching the names
     */
    public static int getQuantity(final String... names) {
        return 0;
    }

    /**
     * Gets the total quantity of items matching the names
     *
     * @param names the ids to check the items against
     * @return the total quantity of items matching the names
     */
    public static int getQuantity(final Pattern... names) {
        return 0;
    }

    public static boolean close() {
        return false;
    }

    public static boolean close(boolean hotkey) {
        return false;
    }

    public static List<InteractableRectangle> getBounds() {
        return Collections.emptyList();
    }

    public static InteractableRectangle getBoundsOf(int index) {
        return null;
    }

    public static SpriteItem getItemIn(final int index) {
        return null;
    }

    public static SpriteItemQueryResults getItems() {
        return new SpriteItemQueryResults(Collections.emptyList());
    }

    public static SpriteItemQueryResults getItems(final Predicate<SpriteItem> filter) {
        return new SpriteItemQueryResults(Collections.emptyList());
    }

    public static boolean isAreaLootEnabled() {
        return false;
    }

    public static boolean isEnabled() {
        return false;
    }

    public static boolean isOpen() {
        return false;
    }

    public static SpriteItemQueryBuilder newQuery() {
        return new SpriteItemQueryBuilder(null);
    }

    public static boolean take(String... names) {
        return false;
    }

    public static boolean take(SpriteItem item) {
        return false;
    }

    @Deprecated
    public static boolean takeAll() {
        return false;
    }

    @Deprecated
    public static boolean takeAll(boolean hotkeys) {
        return false;
    }

    public static boolean lootAll() {
        return false;
    }

    public static boolean lootAll(boolean hotkeys) {
        return false;
    }

    public static boolean lootCustom() {
        return false;
    }

    public static InteractableRectangle getViewport() {
        return null;
    }
}
