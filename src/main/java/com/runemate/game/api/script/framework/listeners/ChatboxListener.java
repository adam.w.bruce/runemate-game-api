package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public interface ChatboxListener extends EventListener {

    void onMessageReceived(MessageEvent event);

}
