package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

/**
 * A Listener for receiving events related directly to the game's engine.
 * It has methods that are fired whenever a new client side logic cycle begins and whenever a new server side logic tick begins.
 */
public interface EngineListener extends EventListener {

    default void onCycleStart() {
    }

    default void onTickStart() {
    }

    default void onEngineStateChanged(EngineStateEvent event) {
    }

}
