package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

/**
 * Receives ALL events dispatched by the EventDispatcher
 */
public interface GlobalListener extends EventListener {

    void onEvent(Event event);
}
