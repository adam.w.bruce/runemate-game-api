package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.location.*;
import lombok.*;

@Value
public class NpcDespawnedEvent implements EntityEvent {

    Coordinate position;
    NpcDefinition lastDefinition;

    @Override
    public EntityType getEntityType() {
        return EntityType.NPC;
    }
}
