package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.*;
import lombok.*;

@Value
public class ProjectileLaunchEvent implements EntityEvent {

    Projectile projectile;

    @Override
    public EntityType getEntityType() {
        return EntityType.PROJECTILE;
    }
}
