package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import lombok.*;

@Value
public class SpotAnimationSpawnEvent implements EntityEvent {

    SpotAnimation spotAnimation;
    Coordinate position;

    @Override
    public EntityType getEntityType() {
        return EntityType.SPOTANIMATION;
    }
}
