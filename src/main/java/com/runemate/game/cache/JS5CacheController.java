package com.runemate.game.cache;

import com.runemate.client.game.open.*;
import com.runemate.game.api.script.data.*;
import com.runemate.game.cache.file.*;
import java.io.*;
import java.nio.file.*;
import java.util.concurrent.*;
import org.jetbrains.annotations.*;

public final class JS5CacheController implements CacheController {
    private static JS5CacheController LARGEST_OSRS_CONTROLLER;
    private static JS5CacheController LARGEST_RS3_CONTROLLER;
    private final long initializationTime;
    private final File directory;
    private final Js5Dat2File dat2File;
    private final ConcurrentMap<Integer, Js5IndexFile> indexFileMap;
    private int revision = -1;

    public JS5CacheController(File directory, boolean acknowledgeSafetyHazard) {
        this(directory);
        if (!acknowledgeSafetyHazard) {
            throw new IllegalStateException(
                "If you're not willing to risk the safety hazard, don't directly initialize.");
        }
    }

    public JS5CacheController(int revision, File directory, boolean acknowledgeSafetyHazard) {
        this(directory);
        this.revision = revision;
        if (!acknowledgeSafetyHazard) {
            throw new IllegalStateException(
                "If you're not willing to risk the safety hazard, don't directly initialize.");
        }
    }

    private JS5CacheController(File directory) {
        this.initializationTime = System.currentTimeMillis();
        this.directory = directory;
        if (!this.directory.exists()) {
            throw new IllegalArgumentException(
                "The directory file must exist \"" + directory.getAbsolutePath() + "\"");
        }
        if (!this.directory.isDirectory()) {
            throw new IllegalArgumentException("The directory file must be a directory.");
        }
        this.dat2File =
            new Js5Dat2File(new File(directory, "main_file_cache.dat2"), StandardOpenOption.READ);
        this.indexFileMap = mapJs5IndexFiles(directory);
        if (getIndexFiles() == null || getIndexFiles().size() == 0) {
            throw new IllegalArgumentException("Unable to identify any js5 cache index files.");
        }
        if (getMetaIndexFile() == null) {
            throw new IllegalArgumentException("The meta tag must correspond to a valid idx file.");
        }
    }

    private static JS5CacheController newJS5CacheController() {
        File largestCacheDirectory = JagCache.getCacheDirectory();
        if (largestCacheDirectory == null) {
            return null;
        }
        return new JS5CacheController(largestCacheDirectory);
    }

    public static JS5CacheController newJs5CacheController() {
        return newJS5CacheController();
    }

    public static JS5CacheController getLargestJS5CacheController() {
        JS5CacheController controller = LARGEST_OSRS_CONTROLLER;
        if (controller == null) {
            controller = newJS5CacheController();
            LARGEST_OSRS_CONTROLLER = controller;
        }
        return controller;
    }

    private ConcurrentMap<Integer, Js5IndexFile> mapJs5IndexFiles(File directory) {
        if (!directory.isDirectory()) {
            return null;
        }
        File[] children = directory.listFiles();
        if (children == null) {
            return null;
        }
        ConcurrentMap<Integer, Js5IndexFile> idxFiles = new ConcurrentHashMap<>(children.length);
        for (File file : children) {
            String name = file.getName();
            if (!name.startsWith("main_file_cache")) {
                continue;
            }
            int extensionIndex = name.lastIndexOf('.');
            if (extensionIndex == -1 || extensionIndex == name.length() - 1) {
                continue;
            }
            String extension = name.substring(extensionIndex + 1);
            if (extension.startsWith("idx")) {
                int index = Integer.parseInt(extension.substring(3));
                idxFiles.put(index, new Js5IndexFile(this, index, file, StandardOpenOption.READ));
            }
        }
        return idxFiles;
    }

    public Js5IndexFile getMetaIndexFile() {
        return getIndexFile(255);
    }

    public Js5IndexFile getIndexFile(int archive) {
        return getIndexFiles().get(archive);
    }

    public File getDirectory() {
        return directory;
    }

    public Js5Dat2File getDat2File() {
        return dat2File;
    }

    public ConcurrentMap<Integer, Js5IndexFile> getIndexFiles() {
        return indexFileMap;
    }

    public long getInitializationTime() {
        return initializationTime;
    }

    public boolean hasChangedSinceInitialization() {
        return directory.lastModified() > initializationTime;
    }

    public int getRevision() {
        if (revision == -1) {
            throw new UnsupportedOperationException(
                "The revision must be set instead of the game type to get the revision.");
        }
        return revision;
    }
}
