package com.runemate.game.cache.item;

import com.runemate.game.cache.io.*;
import java.io.*;
import java.util.*;

public abstract class SerializedFileLoader<D extends DecodedItem> extends FileLoader<D> {
    public SerializedFileLoader(int archive) {
        super(archive);
    }

    protected abstract D construct(int entry, int file, Map<String, Object> arguments);

    @Override
    public D form(int group, int file, byte[] data, Map<String, Object> arguments) throws IOException {
        D item = construct(group, file, arguments);
        if (item != null) {
            item.decode(new Js5InputStream(data));
        }
        return item;
    }
}
