package com.runemate.game.incubating;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.location.*;
import java.util.*;
import lombok.*;

@RequiredArgsConstructor
public class WorldMapEvent {

    private final OpenWorldMapEvent event;

    public int getType() {
        return event.getType();
    }

    public Coordinate getPosition1() {
        final var pos = event.getCoord1();
        return pos != null ? new Coordinate(pos.getX(), pos.getY(), pos.getPlane()) : null;
    }

    public Coordinate getPosition2() {
        final var pos = event.getCoord2();
        return pos != null ? new Coordinate(pos.getX(), pos.getY(), pos.getPlane()) : null;
    }

    public static WorldMapEvent current() {
        final var current = OpenWorldMapEvent.getCurrent();
        return current != null ? new WorldMapEvent(current) : null;
    }

    @Override
    public String toString() {
        return new StringJoiner(",", "WorldMapEvent(", ")")
            .add("type=" + getType())
            .add("position1=" + getPosition1())
            .add("position2=" + getPosition2())
            .toString();
    }
}
