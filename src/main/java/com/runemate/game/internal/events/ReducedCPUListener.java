package com.runemate.game.internal.events;

import static com.runemate.client.game.open.OpenClientPreferences.*;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.script.framework.listeners.*;

public class ReducedCPUListener implements EngineListener {
    private int lowestRenderedPlane = 0;

    @Override
    public void onCycleStart() {
//        setLowDetail(getLowDetailClientSetting());
//        setRenderPolygons(getRenderPolygonsClientSetting());
//        setAnimateEntities(getAnimateEntitiesClientSetting());
        if (getLowDetailClientSetting()) {
//            setHidingRoofs(true);
            int currentPlane = Scene.getCurrentPlane();
            if (currentPlane != lowestRenderedPlane) {
//                setLowestRenderedPlane(currentPlane);
                lowestRenderedPlane = currentPlane;
            }
        } else if (lowestRenderedPlane > 0) {
//            setLowestRenderedPlane(0);
            lowestRenderedPlane = 0;
        }
    }
}