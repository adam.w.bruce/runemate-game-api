package com.runemate.ui;

import com.runemate.game.api.client.embeddable.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.game.internal.*;
import com.runemate.ui.control.*;
import com.runemate.ui.tracker.*;
import java.util.*;
import javafx.animation.*;
import javafx.application.*;
import javafx.beans.property.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.util.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2(topic = "RuneMate")
@Accessors(fluent = true)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DefaultUI implements EmbeddableUI {

    @Getter
    AbstractBot bot;
    @Getter
    StopWatch runtime = new StopWatch();
    @Getter
    LongProperty runtimeProperty = new SimpleLongProperty(0);

    ObjectProperty<ControlPanel> controlPanel;
    Timeline timeline;

    public DefaultUI(@NonNull final AbstractBot bot) {
        this.bot = bot;
        this.runtime.start();

        timeline = new Timeline(new KeyFrame(Duration.millis(100)));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();

        this.controlPanel = new SimpleObjectProperty<>(new ControlPanel(this));
        //Update stopwatch task
        addRepeatTask(() -> {
            runtimeProperty().set(runtime().getRuntime());
            if (bot().isStopped()) {
                timeline.stop();
            }
        });
        //Update bot status task
        addRepeatTask(() -> {
            final var text = (String) bot.getConfiguration().get("bot.status");
            if (text != null) {
                controlPanel.get().setStatusText(text);
            }
        });
    }

    @InternalAPI
    public void addRepeatTask(@NonNull Runnable runnable) {
        timeline.getKeyFrames().add(new KeyFrame(Duration.ZERO, e -> {
            try {
                runnable.run();
            } catch (Throwable t) {
                log.warn("Error when executing repeat task", t);
            }
        }));
    }

    @Override
    @InternalAPI
    public ObjectProperty<? extends Node> botInterfaceProperty() {
        return controlPanel;
    }

    public static void addPanel(@NonNull String title, @NonNull Node content) {
        addPanel(-1, title, content);
    }

    public static void addPanel(int index, @NonNull String title, @NonNull Node content) {
        final var bot = Environment.getBot();
        if (bot != null) {
            addPanel(index, bot, title, content);
        }
    }

    public static void addPanel(@NonNull AbstractBot bot, @NonNull String title, @NonNull Node content) {
        addPanel(-1, bot, title, content, false);
    }

    public static void addPanel(int index, @NonNull AbstractBot bot, @NonNull String title, @NonNull Node content) {
        addPanel(index, bot, title, content, false);
    }

    public static void addPanel(@NonNull AbstractBot bot, @NonNull String title, @NonNull Node content, boolean expanded) {
        addPanel(-1, bot, title, content, expanded);
    }

    public static void addPanel(int index, @NonNull AbstractBot bot, @NonNull String title, @NonNull Node content, boolean expanded) {
        EmbeddableUI embedded = bot.getEmbeddableUI();
        if (embedded instanceof DefaultUI ui) {
            ControlPanel cp = ui.controlPanel.get();
            Platform.runLater(() -> {
                TitledPane pane = new TitledPane(title, content);
                pane.setExpanded(expanded);

                List<Node> children = cp.getContentContainer().getChildren();
                children.add(index < 0 ? children.size() : index, pane);
            });
        }
    }

    public static void setStatus(@NonNull String status) {
        final var bot = Environment.getBot();
        if (bot != null) {
            setStatus(bot, status);
        }
    }

    public static void setStatus(@NonNull AbstractBot bot, @NonNull String status) {
        bot.getConfiguration().put("bot.status", status);
    }

    private static @Nullable ControlPanel getControlPanel(@NonNull AbstractBot bot) {
        final var embeddable = bot.getEmbeddableUI();
        if (embeddable instanceof DefaultUI ui) {
            return ui.controlPanel.get();
        }
        return null;
    }

    public static void setItemEventFilter(@NonNull AbstractBot bot, @NonNull ItemEventFilter filter) {
        final var panel = getControlPanel(bot);
        if (panel != null) {
            panel.getLootPane().setItemEventFilter(filter);
        }
    }

    public static void setItemEventListening(@NonNull AbstractBot bot, boolean enabled) {
        final var panel = getControlPanel(bot);
        if (panel != null) {
            panel.getLootPane().setItemEventListening(enabled);
        }
    }

    public static void submitItemEvent(@NonNull AbstractBot bot, final ItemEvent event) {
        final var panel = getControlPanel(bot);
        if (panel != null) {
            panel.getLootPane().submitItemEvent(event);
        }
    }

}
