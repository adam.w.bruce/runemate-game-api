package com.runemate.ui.control.setting;

import com.runemate.ui.setting.descriptor.open.*;
import java.util.*;
import javafx.beans.binding.*;
import javafx.beans.property.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.util.*;
import lombok.*;

public class SectionPane extends TitledPane {

    @Getter
    private final int order;

    private final Map<Settings.Setting, Pair<Label, Control>> settings;

    public SectionPane(Settings settings, SettingsSectionDescriptor section) {
        setMaxHeight(Integer.MAX_VALUE);
        setPadding(new Insets(4));
        getStyleClass().add("settings-section");
        managedProperty().bind(visibleProperty());

        if (section == null) { //"General" section
            setText("General");
            setExpanded(true);
            setCollapsible(false);
            order = Integer.MIN_VALUE;
        } else { //Named section
            setText(section.title());
            setExpanded(!section.section().collapsed());
            order = section.order();
        }

        var grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setGridLinesVisible(false);
        grid.setPadding(new Insets(4, 4, 4, 24));
        grid.setHgap(8);
        grid.setVgap(4);

        var textConstraints = new ColumnConstraints();
        textConstraints.setHalignment(HPos.RIGHT);
        textConstraints.setFillWidth(true);

        var valueConstraints = new ColumnConstraints();
        valueConstraints.setHalignment(HPos.LEFT);
        valueConstraints.setFillWidth(true);

        grid.getColumnConstraints().addAll(textConstraints, valueConstraints);
        setContent(grid);

        var values = settings.bySection(section);
        this.settings = new HashMap<>();

        var hide = BooleanBinding.booleanExpression(new SimpleBooleanProperty(true));
        for (var entry : values.entrySet()) {
            var setting = entry.getKey();
            var control = entry.getValue();

            var label = new Label(setting.setting().title());
            label.visibleProperty().bind(control.visibleProperty());
            label.managedProperty().bind(label.visibleProperty());

            hide = Bindings.and(hide, control.visibleProperty().not());

            control.visibleProperty().addListener(((observable, oldValue, newValue) -> updateContents()));
            this.settings.put(setting, new Pair<>(label, control));
        }

        visibleProperty().bind(hide.not());
        updateContents();
    }

    private void updateContents() {
        GridPane pane = (GridPane) getContent();
        pane.getChildren().clear();

        var row = 0;
        for (var entry : settings.entrySet().stream().sorted(Map.Entry.comparingByKey(Settings.orderedSettings())).toList()) {
            if (!entry.getValue().getValue().isVisible()) {
                continue;
            }

            pane.add(entry.getValue().getKey(), 0, row);
            pane.add(entry.getValue().getValue(), 1, row);
            row++;
        }
    }
}
