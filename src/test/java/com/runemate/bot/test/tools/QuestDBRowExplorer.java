package com.runemate.bot.test.tools;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.local.*;
import java.util.*;
import lombok.extern.log4j.*;

@Log4j2
public class QuestDBRowExplorer {

    public static void main(String[] args) {
        var quest = Quest.OSRS.DRAGON_SLAYER_I.getDefinition();

        var id = ((CacheQuestDefinition) quest).getRowId();
        var row = DBRows.load(id);

        var types = row.getColumnTypes();
        var values = row.getColumnValues();

        for (int i = 0; i < values.length; i++) {
            var ts = types[i];
            var vs = values[i];

            if (ts == null || vs == null)
                continue;

            if (ts.length != vs.length) {
                log.info("{}: different type length from value length {}/{}", i, ts.length, vs.length);
            }

            StringJoiner joiner = new StringJoiner(", ", i + ": ", "");
            for (int j = 0; j < vs.length; j++) {
                var type = ts[j % ts.length];
                joiner.add(j + " > " + vs[j] + " [" + type + "]");
            }
            log.info(joiner);
        }
    }

}
